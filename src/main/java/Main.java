import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {

    static WebDriver driver;

    @BeforeClass
    public static void setUp(){
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.azercell.com/my/login");
    }

   @AfterClass
   public static void tearDown(){
        driver.close();
   }

    @Test
    public void test1CheckLoginFunctionality() {

        WebElement login = driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-login[1]/v-header[1]/div[1]/div[4]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]"));
        WebElement pass = driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-login[1]/v-header[1]/div[1]/div[4]/div[1]/div[1]/div[1]/form[1]/div[2]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]"));
        WebElement login_btn = driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-login[1]/v-header[1]/div[1]/div[4]/div[1]/div[1]/div[1]/form[1]/div[3]/button[1]"));

        login.sendKeys("518179001");
        pass.sendKeys("a12345");
        login_btn.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        String actualMsisdn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[2]"))).getText();
        Assert.assertEquals("+994518179001",actualMsisdn);
    }

    @Test
    public void test2CheckLanguageEn() {

        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement langEn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[1]/div[1]/div[1]/ul[2]/li[2]/a[1]")));
        langEn.click();
        WebElement actualText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/app-main[1]/div[1]/div[1]/div[1]/div[1]/h3[1]")));
        Assert.assertEquals("Main", actualText.getText());
    }

    @Test
    public void test3CheckLanguageRu() {

        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement langRu = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[1]/div[1]/div[1]/ul[2]/li[3]/a[1]")));
        langRu.click();
        String actualTextRu = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/app-main[1]/div[1]/div[1]/div[1]/div[1]/h3[1]"))).getText();
        Assert.assertEquals("Главная", actualTextRu);
    }

    @Test
    public void test4CheckLanguageAz() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement langAz = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[1]/div[1]/div[1]/ul[2]/li[4]/a[1]")));
        langAz.click();
        String actualTextAz = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/app-main[1]/div[1]/div[1]/div[1]/div[1]/h3[1]"))).getText();
        Assert.assertEquals("Əsas",actualTextAz);
    }

    @Test
    public void test5CheckSendSMSFunction() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement goToSendSMS = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/v-sidenav[1]/div[1]/div[2]/a[1]/img[1]")));
        goToSendSMS.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/app-mobile[1]/div[2]/div[1]/div[2]/form[1]/div[1]/div[1]/div[2]/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]/input[1]"))).sendKeys("518951066");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/app-mobile[1]/div[2]/div[1]/div[2]/form[1]/mat-form-field[1]/div[1]/div[1]/div[1]/textarea[1]"))).sendKeys("This is DEMO SMS");
        driver.findElement(By.xpath("/html[1]/body[1]/app-root[1]/app-entry[1]/v-header[1]/div[1]/div[3]/app-mobile[1]/div[2]/div[1]/div[2]/form[1]/button[1]")).click();
        String alertText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='cdk-overlay-container']"))).getText();
        Assert.assertEquals(alertText,"Əməliyyat uğurla tamamlanmışdır");
    }



}
